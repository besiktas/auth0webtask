'use strict';

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Graham Annett webtask.io project
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

const express = require('express')
const webtask = require('webtask-tools')
const request = require('request-promise')
const app = express()

app.get('/', (req, res) => {
  if (req.query.site) {
    return getData(req.webtaskContext).then(data => {
      console.log('size of current data obj', getSize(data))
      if (data.visited.indexOf(req.query.site) > -1) {
        // we have visited site previously
        return data[req.query.site]
      } else {
        // make the request since we dont have the data
        const options = {
          uri: `https://mercury.postlight.com/parser?url=${req.query.site}`,
          headers: {
            'x-api-key': req.webtaskContext.data.APIKEY
          },
          json: true
        }
        return request(options).then(apiResp => {
          data.visited.push(req.query.site)
          data[req.query.site] = apiResp
          storeData(req.webtaskContext, data)
          return data[req.query.site]
        })
      }

    }).then(siteData => {
      res.send(templateParsed(siteData))
    }).catch(err => {
      console.log('err', err)
    })
  } else {
    res.send(templateMain)
  }
});

// template to use if site not provides in query
const templateMain = `
<html>
<head>
<meta charset=utf-8 />
<title>parse a web page</title>
</head>
<body>
  <h2>enter a site to extract content:</h2>
  <form onSubmit="return getPage();">
  <input id="site" type="text"  class="box" autofocus />
  <input type="submit" class="submit" value="go" />
  </form>
  <script>
  function getPage(){
      var response = document.getElementById('site').value;
      location = location + '?site=' + response
      return false;
  }
  </script>
</body>
</html>
`

/**
 * template for extracted site
 *
 * @param      {object}  data    The data
 * @return     {string}  string to return that has html
 */
const templateParsed = (data) => {
  const site = `
<h2>
title: <u><a href="${data.url}">${data.title}</a></u>
<br/>
author: <u>${data.author}</u>
</h2>
${data.content}
`
  return site
}


/**
 * Gets the size of storage object for 500kb checking.
 *
 * @param      {object}  object
 * @return     {int}  bytes size
 */
function getSize (s) {
  return ~-encodeURI(JSON.stringify(s)).split(/%..|./).length
}

/**
 * Gets the saved data from context.storage
 *
 * @return     {promise}  The data the is retrieved or starer object.
 */
function getData (ctx) {
  return new Promise((resolve, reject) => {
    ctx.storage.get((err, data) => {
      if (err) {
        return reject(err)
      }
      data = (data === undefined) ? { 'visited': [] } : data
      resolve(data)
    })
  })
}

/**
 * Stores data into context.storage
 *
 * @param      {context}   the context
 * @param      {object}   data    The data to store
 */
function storeData(ctx, data) {
  new Promise((resolve, reject) => {
    ctx.storage.set(data, err => (err ? reject(err) : resolve()))
  })
}


module.exports = webtask.fromExpress(app);